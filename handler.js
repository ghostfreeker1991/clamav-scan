const { execSync } = require("child_process");
const { writeFileSync, unlinkSync } = require("fs");
// Future State: require specific aws services, instead of entire aws-sdk
const AWS = require("aws-sdk");

const s3 = new AWS.S3();

module.exports.virusScan = async (event, context) => {
  if (!event.Records) {
    console.log("Not an S3 event invocation!");
    return;
  }

  for (const record of event.Records) {
    if (!record.s3) {
      console.log("Not an S3 Record!");
      continue;
    }
    
    const key = decodeURIComponent(record.s3.object.key.replace(/\+g/, " "));

    console.log('getting the file', key, record);

    // get the file
    const s3Object = await s3
      .getObject({
        Bucket: record.s3.bucket.name,
        Key: record.s3.object.key,
      })
      .promise();
      
    console.log('got the file', record.s3.object.key);

    // get existing tags
    const existing_tags = await s3
      .getObjectTagging({
        Bucket: record.s3.bucket.name,
        Key: record.s3.object.key,
      })
      .promise();
    // creating put operation container
    let new_tagset = [];
    for (i = 0; i < existing_tags["TagSet"].length; i++) {
      new_tagset.push(existing_tags["TagSet"][i]);
    };

    // write file to disk
    writeFileSync(`/tmp/${record.s3.object.key}`, s3Object.Body);

    console.log('wrote the file');
    
    try {
      // scan it
      execSync(`./bin/clamscan --database=./var/lib/clamav /tmp/${record.s3.object.key}`);

      console.log('updating tag to clean');

      new_tagset.push({
        Key: 'av-status',
        Value: 'clean'
      });

      await s3
        .putObjectTagging({
          Bucket: record.s3.bucket.name,
          Key: record.s3.object.key,
          Tagging: {
            TagSet: new_tagset
          }
        })
        .promise();

      console.log('updated tag to clean');
    } catch(err) {
      console.log('err', err);
      if (err.status === 1) {
        // tag as dirty, OR you can delete it
        new_tagset.push({
          Key: 'av-status',
          Value: 'dirty'
        });

        await s3
          .putObjectTagging({
            Bucket: record.s3.bucket.name,
            Key: record.s3.object.key,
            Tagging: {
              TagSet: new_tagset
            }
          })
          .promise();
      }
    }

    // delete the temp file
    unlinkSync(`/tmp/${record.s3.object.key}`);
  }
};
